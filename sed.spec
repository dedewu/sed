Summary: Sed (stream editor) is a non-interactive command-line text editor.

Name: sed
Version: 4.8
Release: 1%{?dist}
License: GPLv3+
URL: https://www.gnu.org/software/sed/

Source0: ftp://ftp.gnu.org/pub/gnu/sed/sed-%{version}.tar.xz

Patch0001: 0001-sed-b-flag.patch
Patch0002: 0002-sed-c-flag.patch
Patch0003: 0003-sed-covscan-annotations.patch

BuildRequires: make automake autoconf gcc glibc-devel libselinux-devel libacl-devel
BuildRequires: perl-Getopt-Long perl(FileHandle)

Provides: /bin/sed
Provides: bundled(gnulib)

%description
Sed is commonly used to filter text, i.e., it takes text input, performs some operation (or set of operations) on it, and outputs the modified text. sed is typically used for extracting part of a file using pattern matching or substituting multiple occurrences of a string within a file.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --without-included-regex
%make_build

%check
make check

%install
%make_install
rm -f ${RPM_BUILD_ROOT}/%{_infodir}/dir

%files
%license COPYING 
%doc BUGS NEWS THANKS README AUTHORS
%{_infodir}/sed.info*
%{_mandir}/man1/sed.1*
%{_datadir}/locale/*
%{_bindir}/sed

%changelog
* Tue Apr 26 2022 jackbertluo <jackbertluo@tencent.com> - 4.8-1 
- Initial build
