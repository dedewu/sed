#!/bin/bash
###############################################################################
# @用例ID: 20220816-005312-301103997
# @用例名称: sed-base
# @用例级别: 1
# @用例标签:
# @用例类型: 功能
###############################################################################
[ -z "$TST_TS_TOPDIR" ] && {
    TST_TS_TOPDIR="$(realpath "$(dirname "$0")/..")"
    export TST_TS_TOPDIR
}
source "${TST_TS_TOPDIR}/tst_common/lib/common.sh" || exit 1
###############################################################################

g_tmpdir="$(mktemp -d)"

tc_setup() {
    msg "this is tc_setup"
    # @预置条件: 系统支持sed命令
    assert_true which sed
    return 0
}

do_test() {
    msg "this is do_test"

    # @测试步骤:1: 验证sed替换字符串的功能
    # @预期结果:1: 替换成功
    str_before="123abc"
    str_after="$(echo "$str_before" | sed "s|[0-9]\+|digit|g")"
    str_expect="digitabc"
    msg "str_before=$str_before, str_after=$str_after, str_expect=$str_expect"
    assert_true [ "$str_after" == "$str_expect" ]

    return 0
}

tc_teardown() {
    msg "this is tc_teardown"
    rm -rfv "$g_tmpdir" || return 1
    return 0
}

###############################################################################
tst_main "$@"
###############################################################################
